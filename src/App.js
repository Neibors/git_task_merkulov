import React from "react"
import { Routes, Route} from "react-router-dom"
 // Написать ссылки на страницы
import HomePage from "./Components/HomePage/HomePage.jsx"
import RusLargestEmployees from "./Components/Tables/RusLargestEmployees/RusLargestEmployees.jsx"


const App = () => {
  return (
      <Routes> 
 <Route path="/" element={<HomePage/>}/>
 <Route path="/employees" element={<RusLargestEmployees/>}/>
      </Routes>
  )
}

export default App;