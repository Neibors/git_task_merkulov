import { useState, useEffect } from "react"
import style from "./RusLargestEmployees.module.scss"
import axios from "axios"
import { Table } from "antd"
import LeftOutlined from "@ant-design/icons/LeftOutlined"

const RusLargestEmployees = () => {
  const [tables, setTables] = useState([])

  useEffect(() => {
    axios({
      method: "GET",
      url: "http://127.0.0.1:8000/api/tables/online-table-chn/",
      headers: {
        "Content-Type": "application/json",
      },
      data: {},
    }).then((response) => {
      setTables(JSON.parse(response.data.replaceAll("NaN", "null")))
    })
  }, [])

  let columns = null
  let data = null

  const columnsCreate = (val = []) => {
    const title = {
      title: "Название",
      width: 255,
      dataIndex: "Название",
      key: "name",
      fixed: "left",
    }
    const objKeys = Object.keys(val[0])
    columns = objKeys.map((el, i) => {
      return i > 0 ? { title: el, dataIndex: el, key: i } : title
    })
    columns[0] = title
    return columns
  }


  const dataCreate = (val = []) => {
    data = val.map((el, i) => {
      const keys = Object.keys(el)
      const values = Object.values(el)
      const allForm = keys.map((el, i) => {
        const newArr = [el, values[i]]
        return newArr
      })
      const newObj = Object.fromEntries(allForm)
      newObj.key = i

      return newObj
    })
    return data
  }

  if (tables.length !== 0) {
    data = dataCreate(tables)
    columns = columnsCreate(tables)
  }

  return (
    <div className={style.container}>
      <div className={style.headMenu}>
        <a href="/">
          <LeftOutlined />
        </a>
        <a href="/">Портфели</a>
      </div>
      <div className={style.h3}>
        Российские компании с наибольшим <br />
        числом работников
      </div>
        <Table columns={columns} dataSource={data} scroll={{ x: 2000 }} />
    </div>
  )
}

export default RusLargestEmployees
