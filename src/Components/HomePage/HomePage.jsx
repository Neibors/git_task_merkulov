import { Routes, Route, Link } from "react-router-dom"
import RusLargestEmployees from "../Tables/RusLargestEmployees/RusLargestEmployees"
import style from "./HomePage.module.scss"
import lock from "../../icon/lock.png"

const HomePage = () => {
  return (
    <div className={style.container}>
      <h2>Портфели</h2>
      <div className={style.tabs_text}>
        <div className={style.tabs_item}>
          <div className={style.unity_txt}>Unity</div>
          <div className={style.active_line} />
        </div>

        <div className={style.tabs_item}>
          <div className={style.ideas_txt}>
            Ideas
            <img src={lock} />
          </div>

          <div className={style.inactive_line} />
        </div>

        <div className={style.bottomLine} />
      </div>
      <div className={style.frame_wrapper}>
        <h4>Россия</h4>
        <div className={style.frame}>
          <Link to="/employees" className={style.link}>
            Российские компании с наибольшим числом работников
          </Link>
          <Link to="/employees" className={style.link}>
            Компании с высокой капитализацией — Рынок акций России
          </Link>
          <Link to="/employees" className={style.link}>
            Российские компании с малой капитализацией
          </Link>
          <Link to="/employees" className={style.link}>
            Эти Акции России приносят высокие дивиденды
          </Link>
        </div>
      </div>

      <div className={style.frame_wrapper}>
      <h4>США</h4>
      <div className={style.frame}>
        <Link to="/employees" className={style.link}>
          Российские компании с наибольшим числом работников
        </Link>
        <Link to="/employees" className={style.link}>
          Компании с высокой капитализацией — Рынок акций России
        </Link>
        <Link to="/employees" className={style.link}>
          Российские компании с малой капитализацией
        </Link>
        <Link to="/employees" className={style.link}>
          Эти Акции России приносят высокие дивиденды
        </Link>
      </div>
    </div>

    <div className={style.frame_wrapper}>
    <h4>Китай</h4>
    <div className={style.frame}>
      <Link to="/employees" className={style.link}>
        Российские компании с наибольшим числом работников
      </Link>
      <Link to="/employees" className={style.link}>
        Компании с высокой капитализацией — Рынок акций России
      </Link>
      <Link to="/employees" className={style.link}>
        Российские компании с малой капитализацией
      </Link>
      <Link to="/employees" className={style.link}>
        Эти Акции России приносят высокие дивиденды
      </Link>
    </div>
  </div>

      

      <Routes>
        <Route path="/employees" element={<RusLargestEmployees />} />
      </Routes>
    </div>
  )
}

export default HomePage
